module Accumulate

let cons x xs = x :: xs

let accumulate func input =
    let rec go func acc =
        function
        | [] -> acc []
        | head :: tail -> go func (acc << (cons (func head))) tail

    go func id input

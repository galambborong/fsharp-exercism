module HighScores

let scores values = values

let latest values = values |> List.last

let personalBest values = values |> List.max

let personalTopThree (values: int list) =
    let n = if values.Length >= 3 then 3 else values.Length
    values |> List.sortDescending |> List.take n

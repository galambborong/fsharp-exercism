module Grains

open Microsoft.FSharp.Core

let square n =
    match n with
    | n when n < 1 || n > 64 -> Error "square must be between 1 and 64"
    | _ -> Ok(pown 2UL (n - 1))

let getResultValue result =
    match result with
    | Ok v -> v
    | Error _ -> failwith "error retrieving value"

let total: Result<uint64, string> =
    [ 1..64 ] |> List.sumBy (fun elem -> square elem |> getResultValue) |> Ok
